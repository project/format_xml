INTRODUCTION
------------

This module gives an interface where you can give the source of XML file and can
read JSON and Array object format of the source XML file.
Basically it convert the XML into JSON and array and shows you the output in a 
field where you can easily debug the code.

It is just taking input as a URL in a field and option to choose one of the 
following options :
1. JSON Format
2. Array Format

It will show the output in the selected option above in a field where you can 
easy copy and paste the code.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/format_xml

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/format_xml


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
