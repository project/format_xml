<?php

/**
 * @file
 * This file having function for converting json to xml and array.
 */

/**
 * Class for FormatXml module.
 */
class FormatXml {

  /**
   * Function for XML to JSON conversion.
   */
  public function xmltojson($url) {
    $filecontents = file_get_contents($url);
    $filecontents = str_replace(array("\n", "\r", "\t"), '', $filecontents);
    $filecontents = trim(str_replace('"', "'", $filecontents));
    $simplexml = simplexml_load_string($filecontents);
    $json = json_encode($simplexml);
    return $json;
  }

  /**
   * Function for XML to Array.
   */
  public function xmltoarr($url) {
    $filecontents = file_get_contents($url);
    $filecontents = str_replace(array("\n", "\r", "\t"), '', $filecontents);
    $filecontents = trim(str_replace('"', "'", $filecontents));
    $simplexml = simplexml_load_string($filecontents);
    $json = json_encode($simplexml);
    $arr = json_decode($json);
    return $arr;
  }

}
