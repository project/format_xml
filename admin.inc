<?php

/**
 * @file
 * This file would have all admin configuration methods.
 */

/**
 * Creating a drupal config form for settings.
 */
function format_xml_data($form, &$form_state) {
  $t = get_t();
  $form = array();
  $dval = variable_get('display_result');
  $option_type = array('0' => '--Select type--', '1' => 'JSON', '2' => 'Array');
  $form['url'] = array(
    '#title' => 'Enter URL',
    '#description' => $t('Enter service URL for converting XML Data in a different format.'),
    '#type' => 'textfield',
    '#default values' => '',
  );
  $form['type'] = array(
    '#title' => 'Convert in',
    '#description' => $t('Select format required to convert xml data'),
    '#type' => 'select',
    '#options' => $option_type,
    '#default values' => 0,

  );
  $form['submit'] = array(
    '#title' => 'Convert',
    '#type' => 'submit',
    '#value' => 'Convert',
  );

  $form['display_result'] = array(
    '#title' => 'Show Result' ,
    '#type' => 'textarea',
    '#value' => print_r($dval, TRUE),
    '#default values' => '',
  );
  return $form;
}

/**
 * Implements form submit.
 */
function format_xml_data_submit($form, &$form_state) {
  $t = get_t();
  if (isset($form_state['values']['submit']) &&  $form_state['values']['submit'] == 'Convert') {
    module_load_include('inc', 'format_xml', '/includes/FormatXml');
    $url = $form_state['values']['url'];
    $formattype = $form_state['values']['type'];
    switch ($formattype) {
      case '0':
      case '1': $res = new FormatXml();
        $result = $res->xmltojson($url);
        break;

      case '2': $resarr = new FormatXml();
        $result = $resarr->xmltoarr($url);
        print_r($result);
        break;
    }

    variable_set('display_result', $result);
    if ($formattype == 1) {
      drupal_set_message($t("Your xml has been converted into JSON successfully."));
    }
    elseif ($formattype == 2) {
      drupal_set_message($t("Your xml has been converted into array successfully."));
    }

    return $form;
  }
}
